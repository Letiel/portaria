<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresas;
use App\Apartamentos;
use App\Visitantes;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Input;

class VisitantesController extends Controller{
    function __construct(){
    	$this->middleware("auth");
    }

    private $rules = array(
        'nome'=>"required|max:100",
        'rg'=>"required|max:100|unique:visitantes,rg",
        'cpf'=>"max:100|unique:visitantes,cpf",
        'endereco'=>"max:200",
        'cidade'=>"max:200",
        'estado'=>"max:200",
        'telefone'=>"max:200",
        'sexo'=>"max:1",
        'data_nascimento'=>"",
        'id_apartamento'=>"numeric",
        'id_empresa'=>"numeric");

    function index(){
        $visitantes = Visitantes::with('apartamento')->with('empresa')->orderby("id", "DESC")->paginate(10);
        return view("visitantes")->with("visitantes", $visitantes);
    }

    function cadastro(){
    	$apartamentos = Apartamentos::all();
    	$empresas = Empresas::all();
    	return view("cadastro-visitante")->with("apartamentos", $apartamentos)->with("empresas", $empresas);
    }

    function create(Request $request){

    	$this->validate($request, $this->rules);

    	$visitante = new Visitantes;
    	$visitante->nome = Input::get('nome');
    	$visitante->rg = Input::get('rg');
    	$visitante->cpf = Input::get('cpf');
    	$visitante->endereco = Input::get('endereco');
    	$visitante->cidade = Input::get('cidade');
    	$visitante->estado = Input::get('estado');
    	$visitante->telefone = Input::get('telefone');
    	$visitante->sexo = Input::get('sexo');
    	$visitante->data_nascimento = date("Y-m-d", strtotime(Input::get('data_nascimento')));
    	$visitante->id_apartamento = Input::get('id_apartamento');
    	$visitante->id_empresa = Input::get('id_empresa');
    	$visitante->situacao = "L";
    	$visitante->observacao = Input::get('observacao');


    	if($visitante->save())
            $request->session()->flash('alert-success', 'Visitante Cadastrado com Sucesso.');
        else
            $request->session()->flash('alert-danger', 'Erro ao cadastrar visitante.');
            
    	return redirect("/visitantes");
    }

    function edicao($id, Request $request){
        $apartamentos = Apartamentos::all();
        $empresas = Empresas::all();
        $visitante = Visitantes::find($id);
        if($visitante != null)
            return view("edicao-visitante")->with("apartamentos", $apartamentos)->with("empresas", $empresas)->with("visitante", $visitante);
        else{
            $request->session()->flash('alert-danger', 'Visitante não encontrado.');
            return redirect("/visitantes");
        }
    }

    function edit($id, Request $request){
        
        $visitante = Visitantes::find($id);
        if($visitante != null){
    
            $rules = $this->rules;
            $rules['rg'] = array(
                    'required',
                    Rule::unique('visitantes')->ignore($id),
                    "max: 100"
                );
            $rules['cpf'] = array(
                    'required',
                    Rule::unique('visitantes')->ignore($id),
                    "max: 100"
                );
            $this->validate($request, $rules);

            // $visitante = new Visitantes;
            // $visitante->id = Input::get('id');
            $visitante->nome = Input::get('nome');
            $visitante->rg = Input::get('rg');
            $visitante->cpf = Input::get('cpf');
            $visitante->endereco = Input::get('endereco');
            $visitante->cidade = Input::get('cidade');
            $visitante->estado = Input::get('estado');
            $visitante->telefone = Input::get('telefone');
            $visitante->sexo = Input::get('sexo');
            $visitante->data_nascimento = date("Y-m-d", strtotime(Input::get('data_nascimento')));
            $visitante->id_apartamento = Input::get('id_apartamento');
            $visitante->id_empresa = Input::get('id_empresa');
            $visitante->situacao = "L";
            $visitante->observacao = Input::get('observacao');


            if($visitante->save())
                $request->session()->flash('alert-success', 'Visitante Alterado com Sucesso.');
            else
                $request->session()->flash('alert-danger', 'Erro ao alterar visitante.');
                
        }else{
            $request->session()->flash('alert-danger', 'Visitante não encontrado.');
        }
        return redirect("/visitantes");
    }

    function delete(Request $request){
        $id = Input::get('id');
        $visitante = Visitantes::find($id);
        if($visitante != null){
            $visitante->delete();
            $request->session()->flash('alert-success', 'Visitante Removido com Sucesso.');
        }else{
            echo "Erro. Registro não encontrado.";
            $request->session()->flash('alert-danger', 'Erro ao remover visitante. O Visitante não foi encontrado.');
        }
    }
}