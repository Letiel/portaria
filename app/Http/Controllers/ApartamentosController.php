<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Apartamentos;
use App\Localizacoes;

class ApartamentosController extends Controller{
	private $rules = array(
        'nome'=>"required|max:100",
        'id_localizacao'=>"required|numeric");

	function __construct(){
    	$this->middleware("auth");
    }
    
    function index(){
    	$apartamentos = Apartamentos::orderby("id", "DESC")->paginate(10);
    	return view("apartamentos")->with("apartamentos", $apartamentos);
    }

    function cadastro(){
    	$localizacoes = Localizacoes::all();
    	return view("cadastro-apartamento")->with("localizacoes", $localizacoes);
    }

    function create(Request $request){
    	$this->validate($request, $this->rules);
    	$apartamento = new Apartamentos;
    	$apartamento->nome = $request->nome;
    	$apartamento->id_localizacao = $request->id_localizacao;

    	if($apartamento->save()){
    		$request->session()->flash('alert-success', 'Apartamento Cadastrado com Sucesso.');
    	}else{
    		$request->session()->flash('alert-danger', 'Erro ao cadastrar apartamento.');
    	}
    	return redirect("/apartamentos");
    }

    function edicao($id, Request $request){
    	return view("edicao-apartamento")->with("localizacoes", Localizacoes::all())->with("apartamento", Apartamentos::find($id));
    }

    function edit($id, Request $request){
    	$apartamento = Apartamentos::find($id);
    	if($apartamento != null){
    		$apartamento->nome = $request->nome;
    		$apartamento->id_localizacao = $request->id_localizacao;

    		if($apartamento->save()){
    			$request->session()->flash('alert-success', 'Apartamento Alterado com Sucesso.');
    		}else{
    			$request->session()->flash('alert-danger', 'Erro ao alterar apartamento.');
    		}
    	}else{
    		$request->session()->flash('alert-danger', 'Apartamento não encontrado.');
    	}
    	return redirect("/apartamentos");
    }

    function delete(Request $request){
    	$apartamento = Apartamentos::find($request->id);
    	if($apartamento != null){
    		$apartamento->delete();
    	}else{
    		echo "Apartamento não encontrado";
    	}
    }
}
