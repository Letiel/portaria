<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

use Validator;
use App\Visitantes;
use App\Visitas;
use App\Empresas;
use App\Usuarios;
use App\Apartamentos;

class VisitasController extends Controller{

	function __construct(){
		$this->middleware("auth");
	}

	private $rules = array(
        'nome'=>"required|max:100",
        'rg'=>"required|max:100|unique:visitantes,rg",
        'cpf'=>"max:100|unique:visitantes,cpf",
        'endereco'=>"max:200",
        'cidade'=>"max:200",
        'estado'=>"max:200",
        'telefone'=>"max:200",
        'sexo'=>"max:1",
        'data_nascimento'=>"",
        'id_apartamento'=>"numeric",
        'id_empresa'=>"numeric");

    function pesquisa(Request $request){
    	$visitante = Visitantes::where("rg", $request->rg)->first();
    	if(!empty($visitante)){
    		echo "<p class='text-success'><b>$visitante->nome</b></p>";
    		echo "<p><button onclick='visitar($visitante->id)' class='btn btn-lg btn-primary'>Registrar Visita</button></p>";
    	}else{
    		echo "<p class='text-danger'>Visitante não encontrado</p>";
    		echo "<script type='text/javascript'>
    			$('.modal-cadastro').modal('show');
    			$('#rg').val('$request->rg');
    		</script>";
    	}
    }

    function cadastro_visitante(Request $request){
    	if(empty($request->cpf)){
    		$this->rules["cpf"] = "";
    	}
    	$validator = Validator::make($request->all(), $this->rules);

    	if ($validator->passes()) {
    		$visitante = Visitantes::where("rg", $request->rg)->first();
    		if($visitante == null){
    			$visitante = new Visitantes;
    			$visitante->nome = $request->nome;
    			$visitante->rg = $request->rg;
    			$visitante->cpf = $request->cpf;
    			$visitante->endereco = $request->endereco;
    			$visitante->cidade = $request->cidade;
    			$visitante->estado = $request->estado;
    			$visitante->telefone = $request->telefone;
    			$visitante->sexo = $request->sexo;
    			if(!empty($request->data_nascimento)){
    				$visitante->data_nascimento =  date("Y-m-d", strtotime($request->data_nascimento));
    			}else{
    				$visitante->data_nascimento = null;
    			}
    			$visitante->id_apartamento = empty($request->id_apartamento) ? null : $request->id_apartamento;
    			$visitante->id_empresa = empty($request->id_empresa) ? null : $request->id_empresa;
    			$visitante->observacao = $request->observacao;
    			$visitante->situacao = "L";

    			if($visitante->save()){
					return response()->json(['success'=>'Visitante Cadastrado.']);
    			}else{
					return response()->json(['error'=>'Erro ao inserir visitante.']);
				}

    		}else{
    			
			return response()->json(['success'=>'Visitante Já Cadastrado.']);
    		}

        }


    	return response()->json(['error'=>$validator->errors()->all()]);
    }

    function visita_empresa(){
    	$empresas = Empresas::all();
    	$apartamentos = Apartamentos::all();
    	return view("visita-empresa", compact("empresas", "apartamentos"));
    }

   	function visitar_empresa(Request $request){
   		$validator = Validator::make($request->all(), array("id_usuario"=>"required|numeric", "id_empresa"=>"required|numeric"));

   		if($validator->passes()){
   			$visita = new Visitas;
   			$visita->id_visitante = $request->id_usuario;
   			$visita->id_empresa = $request->id_empresa;
   			$visita->id_user = \Auth::user()->id;
   			if($visita->save()){
   				$request->session()->flash('alert-success', 'Visita registrada com sucesso.');
   				return response()->json(['success'=>'Visita registrada com sucesso.']);
   			}else{
   				return response()->json(['error'=>'Erro ao registrar visita.']);
   			}
   		}else{
   			return response()->json(['error'=>'Informe todo os campos.']);
   		}
   	}

   	function visita_apartamento(){
    	$empresas = Empresas::all();
    	$apartamentos = Apartamentos::all();
    	return view("visita-apartamento")->with("empresas", $empresas)->with("apartamentos", $apartamentos);
    }

   	function visitar_apartamento(Request $request){
   		$validator = Validator::make($request->all(), array("id_usuario"=>"required|numeric", "id_apartamento"=>"required|numeric"));

   		if($validator->passes()){
   			$visita = new Visitas;
   			$visita->id_visitante = $request->id_usuario;
   			$visita->id_apartamento = $request->id_apartamento;
   			$visita->id_user = \Auth::user()->id;
   			if($visita->save()){
   				$request->session()->flash('alert-success', 'Visita registrada com sucesso.');
   				return response()->json(['success'=>'Visita registrada com sucesso.']);
   			}else{
   				return response()->json(['error'=>'Erro ao registrar visita.']);
   			}
   		}else{
   			return response()->json(['error'=>'Informe todo os campos.']);
   		}
   	}

   	function visita_pessoa(){
    	$empresas = Empresas::all();
    	$apartamentos = Apartamentos::all();
    	$usuarios = Usuarios::all();
    	return view("visita-pessoa")->with("empresas", $empresas)->with("apartamentos", $apartamentos)->with("usuarios", $usuarios);
    }

   	function visitar_pessoa(Request $request){
   		$validator = Validator::make($request->all(), array("id_usuario"=>"required|numeric", "id_pessoa"=>"required|numeric"));

   		if($validator->passes()){
   			$visita = new Visitas;
   			$visita->id_visitante = $request->id_usuario;
   			$visita->id_usuario = $request->id_pessoa;
   			$visita->id_user = \Auth::user()->id;
   			if($visita->save()){
   				$request->session()->flash('alert-success', 'Visita registrada com sucesso.');
   				return response()->json(['success'=>'Visita registrada com sucesso.']);
   			}else{
   				return response()->json(['error'=>'Erro ao registrar visita.']);
   			}
   		}else{
   			return response()->json(['error'=>'Informe todo os campos.']);
   		}
   	}

    function getEmpresa(Request $request){
    	$empresa = DB::table("empresas")
    	    	->select("empresas.nome", "empresas.id_apartamento", "apartamentos.nome as apartamento", "localizacoes.nome as localizacao")
    	    	->leftJoin("apartamentos", "apartamentos.id", "empresas.id_apartamento")
    	    	->leftJoin("localizacoes", "localizacoes.id", "apartamentos.id_localizacao")
    	    	->where("empresas.id", "$request->id");
    	if($empresa->count() > 0){
    		$empresa = $empresa->first();
    		return response()->json(['success'=>$empresa]);
    	}else{
    		return response()->json(['error'=>"Empresa não encontrada."]);
    	}
    }

    function getApartamento(Request $request){
    	$apartamento = DB::table("apartamentos")
    	    	->select("apartamentos.nome", "apartamentos.id_localizacao", "localizacoes.nome as localizacao")
    	    	->leftJoin("localizacoes", "localizacoes.id", "apartamentos.id_localizacao")
    	    	->where("apartamentos.id", "$request->id");
    	if($apartamento->count() > 0){
    		$apartamento = $apartamento->first();
    		return response()->json(['success'=>$apartamento]);
    	}else{
    		return response()->json(['error'=>"Apartamento não encontrado."]);
    	}
    }

    function getPessoa(Request $request){
    	$pessoa = DB::table("usuarios")
    	    	->select("usuarios.nome", "usuarios.id_apartamento","usuarios.id_empresa", "localizacoes.nome as localizacao", "apartamentos.nome as apartamento", "empresas.nome as empresa")
    	    	->leftJoin("apartamentos", "apartamentos.id", "usuarios.id_apartamento")
    	    	->leftJoin("empresas", "empresas.id", "usuarios.id_empresa")
    	    	->leftJoin("localizacoes", "localizacoes.id", "apartamentos.id_localizacao")
    	    	->where("usuarios.id", "$request->id");
    	if($pessoa->count() > 0){
    		$pessoa = $pessoa->first();
    		return response()->json(['success'=>$pessoa]);
    	}else{
    		return response()->json(['error'=>"Pessoa não encontrada."]);
    	}
    }

    function relatorio_visitas(){
    	// $visitas = Visitas::orderby("id", "DESC")->paginate(10);

    	$visitas = DB::table("visitas")
    	    	->select("visitas.id_usuario", "visitas.id_apartamento", "visitas.id_empresa", "usuarios_visita.nome as usuario_visita", "apartamentos_visita.nome as apartamento_visita", "empresas_visita.nome as empresa_visita", "apartamentos_usuario.nome as apartamento_usuario", "empresas_usuario.nome as empresa_usuario", "apartamentos_empresa.nome as apartamento_empresa", "localizacoes_empresa.nome as localizacao_empresa", "visitantes.nome as visitante", "localizacoes_apartamento.nome as localizacao_apartamento", "localizacoes_usuario.nome as localizacao_usuario", "visitas.created_at as data", "users.name as user")
    	    	->leftJoin("users", "users.id", "visitas.id_user")
    	    	->leftJoin("visitantes", "visitantes.id", "visitas.id_visitante")
    	    	// VISITAS
    	    	->leftJoin("usuarios as usuarios_visita", "usuarios_visita.id", "visitas.id_usuario")
    	    	->leftJoin("apartamentos as apartamentos_visita", "apartamentos_visita.id", "visitas.id_apartamento")
    	    	->leftJoin("empresas as empresas_visita", "empresas_visita.id", "visitas.id_empresa")
    	    	// VISITAS

    	    	// USUARIOS
    	    	->leftJoin("apartamentos as apartamentos_usuario", "apartamentos_usuario.id", "usuarios_visita.id_apartamento")
    	    	->leftJoin("empresas as empresas_usuario", "empresas_usuario.id", "usuarios_visita.id_empresa")
    	    	->leftJoin("localizacoes as localizacoes_usuario", "localizacoes_usuario.id", "apartamentos_usuario.id_localizacao")
    	    	// USUARIOS

    	    	// EMPRESAS
    	    	->leftJoin("apartamentos as apartamentos_empresa", "apartamentos_empresa.id", "empresas_visita.id_apartamento")
    	    	->leftJoin("localizacoes as localizacoes_empresa", "localizacoes_empresa.id", "apartamentos_empresa.id_localizacao")
    	    	// EMPRESAS

    	    	// EMPRESAS
    	    	->leftJoin("localizacoes as localizacoes_apartamento", "localizacoes_apartamento.id", "apartamentos_visita.id_localizacao")
    	    	// EMPRESAS
    	    	
    	    	->orderby("visitas.id", "DESC")->paginate(10);

    	return view("relatorio-visitas")->with("visitas", $visitas);
    }
}
