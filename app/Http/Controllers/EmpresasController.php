<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresas;
use App\Apartamentos;

class EmpresasController extends Controller{
    private $rules = array(
        'nome'=>"required|max:100",
        'id_apartamento'=>"required|numeric");

    function __construct(){
    	$this->middleware("auth");
    }

    function index(){
    	$empresas = Empresas::orderby("id", "DESC")->paginate(10);
    	return view("empresas")->with("empresas", $empresas);
    }

    function cadastro(){
    	$apartamentos = Apartamentos::all();
    	return view("cadastro-empresa")->with("apartamentos", $apartamentos);
    }

    function create(Request $request){
    	$this->validate($request, $this->rules);
    	$empresa = new Empresas;
    	$empresa->nome = $request->nome;
    	$empresa->id_apartamento = $request->id_apartamento;

    	if($empresa->save()){
    		$request->session()->flash('alert-success', 'Empresa Cadastrada com Sucesso.');
    	}else{
    		$request->session()->flash('alert-danger', 'Erro ao cadastrar empresa.');
    	}
    	return redirect("/empresas");
    }

    function edicao($id, Request $request){
    	return view("edicao-empresa")->with("apartamentos", Apartamentos::all())->with("empresa", Empresas::find($id));
    }

    function edit($id, Request $request){
    	$empresa = Empresas::find($id);
    	if($empresa != null){
    		$empresa->nome = $request->nome;
    		$empresa->id_apartamento = $request->id_apartamento;

    		if($empresa->save()){
    			$request->session()->flash('alert-success', 'Empresa Alterado com Sucesso.');
    		}else{
    			$request->session()->flash('alert-danger', 'Erro ao alterar empresa.');
    		}
    	}else{
    		$request->session()->flash('alert-danger', 'Empresa não encontrada.');
    	}
    	return redirect("/empresas");
    }

    function delete(Request $request){
    	$empresa = Empresas::find($request->id);
    	if($empresa != null){
    		$empresa->delete();
    	}else{
    		echo "Empresa não encontrada";
    	}
    }
}
