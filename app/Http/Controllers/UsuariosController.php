<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Apartamentos;
use App\Localizacoes;
use App\Usuarios;
use App\Empresas;

class UsuariosController extends Controller{
    private $rules = array(
        'nome'=>"required|max:100",
        'id_apartamento'=>"numeric",
        'id_empresa'=>"numeric",);

    function __construct(){
    	$this->middleware("auth");
    }

    function index(){
    	$usuarios = Usuarios::orderby("id", "DESC")->paginate(10);
    	return view("usuarios")->with("usuarios", $usuarios);
    }

    function cadastro(){
    	$apartamentos = Apartamentos::all();
    	$empresas = Empresas::all();
    	return view("cadastro-usuario")->with("empresas", $empresas)->with("apartamentos", $apartamentos);
    }

    function create(Request $request){
    	$this->validate($request, $this->rules);
    	$usuario = new Usuarios;
    	$usuario->nome = $request->nome;
    	$usuario->id_apartamento = empty($request->id_apartamento) ? null : $request->id_apartamento;
    	$usuario->id_empresa = empty($request->id_empresa) ? null : $request->id_empresa;

    	if($usuario->save()){
    		$request->session()->flash('alert-success', 'Usuário Cadastrado com Sucesso.');
    	}else{
    		$request->session()->flash('alert-danger', 'Erro ao cadastrar usuário.');
    	}
    	return redirect("/usuarios");
    }

    function edicao($id, Request $request){
    	$apartamentos = Apartamentos::all();
    	$empresas = Empresas::all();
    	return view("edicao-usuario")->with("usuario", Usuarios::find($id))->with("empresas", $empresas)->with("apartamentos", $apartamentos);
    }

    function edit($id, Request $request){
    	$usuario = Usuarios::find($id);
    	if($usuario != null){
    		$usuario->nome = $request->nome;

    		if($usuario->save()){
    			$request->session()->flash('alert-success', 'Usuário Alterado com Sucesso.');
    		}else{
    			$request->session()->flash('alert-danger', 'Erro ao alterar usuário.');
    		}
    	}else{
    		$request->session()->flash('alert-danger', 'Usuário não encontrado.');
    	}
    	return redirect("/usuarios");
    }

    function delete(Request $request){
    	$usuario = Usuarios::find($request->id);
    	if($usuario != null){
    		$usuario->delete();
    	}else{
    		echo "Usuário não encontrado";
    	}
    }
}
