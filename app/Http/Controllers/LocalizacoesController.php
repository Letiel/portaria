<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Localizacoes;

class LocalizacoesController extends Controller{
    private $rules = array(
        'nome'=>"required|max:100");

    function __construct(){
    	$this->middleware("auth");
    }

    function index(){
    	$localizacoes = Localizacoes::orderby("id", "DESC")->paginate(10);
    	return view("localizacoes")->with("localizacoes", $localizacoes);
    }

    function cadastro(){
    	return view("cadastro-localizacao");
    }

    function create(Request $request){
    	$this->validate($request, $this->rules);
    	$localizacao = new Localizacoes;
    	$localizacao->nome = $request->nome;

    	if($localizacao->save()){
    		$request->session()->flash('alert-success', 'Localização Cadastrada com Sucesso.');
    	}else{
    		$request->session()->flash('alert-danger', 'Erro ao cadastrar localização.');
    	}
    	return redirect("/localizacoes");
    }

    function edicao($id, Request $request){
    	return view("edicao-localizacao")->with("localizacao", Localizacoes::find($id));
    }

    function edit($id, Request $request){
    	$localizacao = Localizacoes::find($id);
    	if($localizacao != null){
    		$localizacao->nome = $request->nome;

    		if($localizacao->save()){
    			$request->session()->flash('alert-success', 'Localização Alterada com Sucesso.');
    		}else{
    			$request->session()->flash('alert-danger', 'Erro ao alterar localização.');
    		}
    	}else{
    		$request->session()->flash('alert-danger', 'Localização não encontrada.');
    	}
    	return redirect("/localizacoes");
    }

    function delete(Request $request){
    	$localizacao = Localizacoes::find($request->id);
    	if($localizacao != null){
    		$localizacao->delete();
    	}else{
    		echo "Localização não encontrada";
    	}
    }
}
