<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;



    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function validateLogin(Request $request){
        $validation = $this->validate($request, [
          'email' => 'required',
          'password' => 'required',
          // new rules here
          ],
          [
          'email.required' => "Informe seu login ou email",
          'password.required' => "Informe uma senha",
          ]);
    }

    protected function attemptLogin(Request $request){
        $email = $request->get("email");
        $password = $request->get("password");

        return \Auth::attempt([
            filter_var($email, FILTER_VALIDATE_EMAIL) ? 'email' : 'name' => $email,
            'password' => $password
        ]);
    }

    protected function authenticated(Request $request){

        $password = $request->input('password');
        $email = $request->input('email');

        if (Auth::attempt(['email' => $email, 'password' => $password]) || Auth::attempt(['name' => $email, 'password' => $password])){
            return redirect("/home");
        }

        return redirect('/');
    }
}
