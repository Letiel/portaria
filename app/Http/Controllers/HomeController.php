<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Apartamentos;
use App\Empresas;
use App\Usuarios;
use App\Visitas;

class HomeController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){
    	$ultimo = DB::table("visitas")
    	    	->select("visitas.id_usuario", "visitas.id_apartamento", "visitas.id_empresa", "usuarios_visita.nome as usuario_visita", "apartamentos_visita.nome as apartamento_visita", "empresas_visita.nome as empresa_visita", "apartamentos_usuario.nome as apartamento_usuario", "empresas_usuario.nome as empresa_usuario", "apartamentos_empresa.nome as apartamento_empresa", "localizacoes_empresa.nome as localizacao_empresa", "visitantes.nome as visitante", "localizacoes_apartamento.nome as localizacao_apartamento", "localizacoes_usuario.nome as localizacao_usuario")
    	    	->leftJoin("visitantes", "visitantes.id", "visitas.id_visitante")
    	    	// VISITAS
    	    	->leftJoin("usuarios as usuarios_visita", "usuarios_visita.id", "visitas.id_usuario")
    	    	->leftJoin("apartamentos as apartamentos_visita", "apartamentos_visita.id", "visitas.id_apartamento")
    	    	->leftJoin("empresas as empresas_visita", "empresas_visita.id", "visitas.id_empresa")
    	    	// VISITAS

    	    	// USUARIOS
    	    	->leftJoin("apartamentos as apartamentos_usuario", "apartamentos_usuario.id", "usuarios_visita.id_apartamento")
    	    	->leftJoin("empresas as empresas_usuario", "empresas_usuario.id", "usuarios_visita.id_empresa")
    	    	->leftJoin("localizacoes as localizacoes_usuario", "localizacoes_usuario.id", "apartamentos_usuario.id_localizacao")
    	    	// USUARIOS

    	    	// EMPRESAS
    	    	->leftJoin("apartamentos as apartamentos_empresa", "apartamentos_empresa.id", "empresas_visita.id_apartamento")
    	    	->leftJoin("localizacoes as localizacoes_empresa", "localizacoes_empresa.id", "apartamentos_empresa.id_localizacao")
    	    	// EMPRESAS

    	    	// EMPRESAS
    	    	->leftJoin("localizacoes as localizacoes_apartamento", "localizacoes_apartamento.id", "apartamentos_visita.id_localizacao")
    	    	// EMPRESAS
    	    	
    	    	->orderby("visitas.id", "DESC")->first();
        return view('home')->with("apartamentos", Apartamentos::all())->with("empresas", Empresas::all())->with("usuarios", Usuarios::all())->with("ultimo", $ultimo);
    }
}