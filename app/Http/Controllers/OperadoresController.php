<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Operadores;
use App\Apartamentos;

class OperadoresController extends Controller{
	private $rules = array(
        'nome'=>"required|max:100|min:4",
        'email'=>"required|max:100|min:4|email",
        'senha'=>"required|max:100|min:4|same:confirmacao",
        'confirmacao'=>"required|max:100|min:4",
        );

    function __construct(){
    	$this->middleware("auth");
    }

    function index(){
    	return view("operadores")->with("operadores", Operadores::orderby("id", "DESC")->paginate(10));
    }

    function cadastro(){
    	return view("cadastro-operador");
    }

    function create(Request $request){

    	$this->validate($request, $this->rules);
    	$operador = new Operadores;
    	$operador->name = $request->nome;
    	$operador->password = Hash::make($request->senha);
    	$operador->email = $request->email;


    	if($operador->save())
            $request->session()->flash('alert-success', 'Operador Cadastrado com Sucesso.');
        else
            $request->session()->flash('alert-danger', 'Erro ao cadastrar operador.');
            
    	return redirect("/operadores");
    }

    function edicao($id, Request $request){
        $operador = Operadores::find($id);
        if($operador != null)
            return view("edicao-operador")->with("operador", $operador);
        else{
            $request->session()->flash('alert-danger', 'Operador não encontrado.');
            return redirect("/operadores");
        }
    }

    function edit($id, Request $request){
        
        $operador = Operadores::find($id);
        if($operador != null){
    
            $rules = $this->rules;
            
            if(empty($request->senha) && empty($request->confirmacao)){
            	$rules["senha"] = "";
            	$rules["confirmacao"] = "";
            }
            $this->validate($request, $rules);

            // $visitante = new Visitantes;
            // $visitante->id = Input::get('id');
            $operador->name = $request->nome;
            if(!empty($request->senha))
	    		$operador->password = Hash::make($request->senha);
	    	$operador->email = $request->email;


            if($operador->save())
                $request->session()->flash('alert-success', 'Operador Alterado com Sucesso.');
            else
                $request->session()->flash('alert-danger', 'Erro ao alterar operador.');
                
        }else{
            $request->session()->flash('alert-danger', 'Operador não encontrado.');
        }
        return redirect("/operadores");
    }

    function delete(Request $request){
        $id = Input::get('id');
        $operador = Operadores::find($id);
        if($operador != null){
            $operador->delete();
            $request->session()->flash('alert-success', 'Operador Removido com Sucesso.');
        }else{
            echo "Erro. Registro não encontrado.";
            $request->session()->flash('alert-danger', 'Erro ao remover operador. O operador não foi encontrado.');
        }
    }
}
