<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitantes extends Model{
	public function apartamento(){
		return $this->hasMany("App\Apartamentos", "id", "id_apartamento");
	}
	public function empresa(){
		return $this->hasMany("App\Empresas", "id", "id_empresa");
	}
}
