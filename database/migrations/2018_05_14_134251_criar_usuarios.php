<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->string("name", 50);
            $table->string("password", 70);
            $table->string("email", 50);
            $table->string("remember_token", 100)->default(NULL);
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::create("localizacoes", function(Blueprint $table){
            $table->bigincrements("id");
            $table->string("nome", 50);
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::create("apartamentos", function(Blueprint $table){
            $table->bigincrements("id");
            $table->string("nome", 50);
            $table->unsignedBigInteger("id_localizacao");
            $table->foreign('id_localizacao')
                            ->references('id')->on('localizacoes')
                            ->onDelete('cascade');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::create("empresas", function(Blueprint $table){
            $table->bigincrements("id");
            $table->string("nome", 50);
            $table->unsignedBigInteger("id_apartamento");
            $table->foreign('id_apartamento')
                            ->references('id')->on('apartamentos')
                            ->onDelete('cascade');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::create("usuarios", function(Blueprint $table){
            $table->bigincrements("id");
            $table->string("nome", 50);
            $table->unsignedBigInteger("id_apartamento");
            $table->unsignedBigInteger("id_empresa");
            $table->foreign('id_apartamento')
                            ->references('id')->on('apartamentos')
                            ->onDelete('cascade');
            $table->foreign('id_empresa')
                            ->references('id')->on('empresas')
                            ->onDelete('cascade');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::create("visitantes", function(Blueprint $table){
            $table->bigincrements("id");
            $table->string("nome", 50);
            $table->string("rg", 50)->unique();
            $table->string("cpf", 50)->unique();
            $table->string("endereco", 200);
            $table->string("cidade", 50);
            $table->string("estado", 50);
            $table->string("telefone", 20);
            $table->char("situacao");
            $table->text("observacao");
            $table->char("sexo", 1);
            $table->date("data_nascimento");
            $table->unsignedBigInteger("id_apartamento");
            $table->unsignedBigInteger("id_empresa");
            $table->foreign('id_apartamento')
                            ->references('id')->on('apartamentos')
                            ->onDelete('cascade');
            $table->foreign('id_empresa')
                            ->references('id')->on('empresas')
                            ->onDelete('cascade');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::create("visitas", function(Blueprint $table){
            $table->bigincrements("id");
            $table->unsignedBigInteger("id_visitante");
            $table->unsignedBigInteger("id_usuario");
            $table->unsignedBigInteger("id_empresa");
            $table->unsignedBigInteger("id_apartamento");
            $table->unsignedBigInteger("id_user");
            $table->foreign('id_visitante')
                            ->references('id')->on('visitantes');
            $table->foreign('id_empresa')
                            ->references('id')->on('empresas');
            $table->foreign('id_usuario')
                            ->references('id')->on('usuarios');
            $table->foreign('id_apartamento')
                            ->references('id')->on('apartamentos');
            $table->foreign('id_user')
                            ->references('id')->on('users');
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('localizacoes');
        Schema::dropIfExists('apartamentos');
        Schema::dropIfExists('empresas');
        Schema::dropIfExists('usuarios');
        Schema::dropIfExists('visitantes');
        Schema::dropIfExists('visitas');
    }
}
