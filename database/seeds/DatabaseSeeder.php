<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Localizacoes;
use App\Empresas;
use App\Apartamentos;

use Faker\Generator;

class DatabaseSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	$faker = Faker\Factory::create();
        DB::table('localizacoes')->insert([
            'nome' => str_random(10),
        ]);

        $localizacoes = Localizacoes::all()->pluck('id')->toArray();
        DB::table('apartamentos')->insert([
            'nome' => str_random(10),
            'id_localizacao'=>$faker->randomElement($localizacoes)
        ]);

        $apartamentos = Apartamentos::all()->pluck('id')->toArray();
        DB::table('empresas')->insert([
            'nome' => str_random(10),
            'id_apartamento'=>$faker->randomElement($apartamentos)
        ]);

        // $empresas = Empresas::all()->pluck('id')->toArray();
        // DB::table('empresas')->insert([
        //     'nome' => str_random(10),
        //     'id_localizacao'=>$faker->randomElement($localizacoes)
        // ]);
    }
}
