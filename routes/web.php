<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/pesquisa');
});

Auth::routes();

Route::get('/pesquisa', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('/visitantes', 'VisitantesController@index');
Route::get('/cadastro-visitante', 'VisitantesController@cadastro');
Route::post('/cadastro-visitante', 'VisitantesController@create');
Route::get('/edicao-visitante/{id}', 'VisitantesController@edicao');
Route::post('/edicao-visitante/{id}', 'VisitantesController@edit');
Route::post('/deletar-visitante', 'VisitantesController@delete');

Route::get('/apartamentos', 'ApartamentosController@index');
Route::get('/cadastro-apartamento', 'ApartamentosController@cadastro');
Route::post('/cadastro-apartamento', 'ApartamentosController@create');
Route::get('/edicao-apartamento/{id}', 'ApartamentosController@edicao');
Route::post('/edicao-apartamento/{id}', 'ApartamentosController@edit');
Route::post('/deletar-apartamento', 'ApartamentosController@delete');

Route::get('/empresas', 'EmpresasController@index');
Route::get('/cadastro-empresa', 'EmpresasController@cadastro');
Route::post('/cadastro-empresa', 'EmpresasController@create');
Route::get('/edicao-empresa/{id}', 'EmpresasController@edicao');
Route::post('/edicao-empresa/{id}', 'EmpresasController@edit');
Route::post('/deletar-empresa', 'EmpresasController@delete');

Route::get('/localizacoes', 'LocalizacoesController@index');
Route::get('/cadastro-localizacao', 'LocalizacoesController@cadastro');
Route::post('/cadastro-localizacao', 'LocalizacoesController@create');
Route::get('/edicao-localizacao/{id}', 'LocalizacoesController@edicao');
Route::post('/edicao-localizacao/{id}', 'LocalizacoesController@edit');
Route::post('/deletar-localizacao', 'LocalizacoesController@delete');

Route::get('/usuarios', 'UsuariosController@index');
Route::get('/cadastro-usuario', 'UsuariosController@cadastro');
Route::post('/cadastro-usuario', 'UsuariosController@create');
Route::get('/edicao-usuario/{id}', 'UsuariosController@edicao');
Route::post('/edicao-usuario/{id}', 'UsuariosController@edit');
Route::post('/deletar-usuario', 'UsuariosController@delete');

Route::get('/operadores', 'OperadoresController@index');
Route::get('/cadastro-operador', 'OperadoresController@cadastro');
Route::post('/cadastro-operador', 'OperadoresController@create');
Route::get('/edicao-operador/{id}', 'OperadoresController@edicao');
Route::post('/edicao-operador/{id}', 'OperadoresController@edit');
Route::post('/deletar-operador', 'OperadoresController@delete');

Route::post('/pesquisa', 'VisitasController@pesquisa');
Route::post('/cadastro-visitante-ajax', 'VisitasController@cadastro_visitante');
Route::post('/visitar', 'VisitasController@visitar');
Route::post('/getUsuario', 'VisitasController@getUsuario');
Route::post('/getEmpresa', 'VisitasController@getEmpresa');
Route::post('/getApartamento', 'VisitasController@getApartamento');
Route::post('/getPessoa', 'VisitasController@getPessoa');

Route::get('/visita-empresa', 'VisitasController@visita_empresa');
Route::post('/visitar-empresa', 'VisitasController@visitar_empresa');

Route::get('/visita-apartamento', 'VisitasController@visita_apartamento');
Route::post('/visitar-apartamento', 'VisitasController@visitar_apartamento');

Route::get('/visita-pessoa', 'VisitasController@visita_pessoa');
Route::post('/visitar-pessoa', 'VisitasController@visitar_pessoa');

Route::get('/relatorio-visitas', 'VisitasController@relatorio_visitas');