<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edição de Visitantes</div>
                <div class="panel-body">
                    <form action="" method="post">
                    	<?php echo e(csrf_field()); ?>

                    	<div class="form-group">
                    		<label for="nome">Nome</label>
                            <input type="hidden" name="id" value="<?php echo e($visitante->id); ?>" style="display: none;" />
                    		<input value="<?php echo e($visitante->nome); ?>" class="form-control" name="nome" id="nome" placeholder="Nome" />
                            <p class="text-danger">
                                <?php if($errors->has('nome')): ?>
                                    <?php echo e($errors->first('nome')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="rg">RG</label>
                    		<input value="<?php echo e($visitante->rg); ?>" class="form-control" name="rg" id="rg" placeholder="RG" />
                            <p class="text-danger">
                                <?php if($errors->has('rg')): ?>
                                    <?php echo e($errors->first('rg')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="cpf">CPF</label>
                    		<input value="<?php echo e($visitante->cpf); ?>" class="form-control" name="cpf" id="cpf" placeholder="CPF" />
                            <p class="text-danger">
                                <?php if($errors->has('cpf')): ?>
                                    <?php echo e($errors->first('cpf')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="endereco">Endereço</label>
                    		<input value="<?php echo e($visitante->endereco); ?>" class="form-control" name="endereco" id="endereco" placeholder="Endereço" />
                            <p class="text-danger">
                                <?php if($errors->has('endereco')): ?>
                                    <?php echo e($errors->first('endereco')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="cidade">Cidade</label>
                    		<input value="<?php echo e($visitante->cidade); ?>" class="form-control" name="cidade" id="cidade" placeholder="Cidade" />
                            <p class="text-danger">
                                <?php if($errors->has('cidade')): ?>
                                    <?php echo e($errors->first('cidade')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="estado">Estado</label>
                    		<input value="<?php echo e($visitante->estado); ?>" class="form-control" name="estado" id="estado" placeholder="Estado" />
                            <p class="text-danger">
                                <?php if($errors->has('estado')): ?>
                                    <?php echo e($errors->first('estado')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="telefone">Telefone</label>
                    		<input value="<?php echo e($visitante->telefone); ?>" class="form-control" name="telefone" id="telefone" placeholder="Telefone" />
                            <p class="text-danger">
                                <?php if($errors->has('telefone')): ?>
                                    <?php echo e($errors->first('telefone')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="sexo">Sexo</label>
                    		<select class="form-control" name="sexo" id="sexo">
                    			<option <?php echo e($visitante->sexo == 'M' ? 'selected' : ''); ?> value="M">Masculino</option>
                    			<option <?php echo e($visitante->sexo == 'F' ? 'selected' : ''); ?> value="F">Feminino</option>
                    		</select>
                            <p class="text-danger">
                                <?php if($errors->has('sexo')): ?>
                                    <?php echo e($errors->first('sexo')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="data_nascimento">Data de nascimento</label>
                    		<input value="<?php echo e($visitante->data_nascimento); ?>" class="form-control" name="data_nascimento" id="data_nascimento" placeholder="__/__/____" />
                            <p class="text-danger">
                                <?php if($errors->has('data_nascimento')): ?>
                                    <?php echo e($errors->first('data_nascimento')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>

                    	<div class="form-group">
                    		<label for="apartamento">Apartamento</label>
                    		<select class="form-control" id="apartamento" name="id_apartamento">
                    			<option value="">Selecione</option>
                    			<?php $__currentLoopData = $apartamentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $apartamento): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    				<option <?php echo e($visitante->id_apartamento == $apartamento->id ? 'selected' : ''); ?> value="<?php echo e($apartamento->id); ?>"><?php echo e($apartamento->nome); ?></option>
                    			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    		</select>
                            <p class="text-danger">
                                <?php if($errors->has('id_apartamento')): ?>
                                    <?php echo e($errors->first('id_apartamento')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="empresa">Empresa</label>
                    		<select class="form-control" id="empresa" name="id_empresa">
                    			<option value="">Selecione</option>
                    			<?php $__currentLoopData = $empresas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empresa): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    				<option <?php echo e($visitante->id_empresa == $empresa->id ? 'selected' : ''); ?> value="<?php echo e($empresa->id); ?>"><?php echo e($empresa->nome); ?></option>
                    			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    		</select>
                            <p class="text-danger">
                                <?php if($errors->has('id_empresa')): ?>
                                    <?php echo e($errors->first('id_empresa')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>
                        <div class="form-group">
                            <label for="observacao">Observação</label>
                            <textarea class="form-control" id="observacao" name="observacao"><?php echo e($visitante->observacao); ?></textarea>
                            <p class="text-danger">
                                <?php if($errors->has('observacao')): ?>
                                    <?php echo e($errors->first('observacao')); ?>

                                <?php endif; ?>
                            </p>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(url('/js/jquery.mask.min.js')); ?>"></script>
<script type="text/javascript">
    $("#cpf").mask("000.000.000-00");
    $("#telefone").mask("(00) 90000 0000");
    $("#data_nascimento").mask("00/00/0000");
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>