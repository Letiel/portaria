<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Visitantes</div>
                <div class="panel-body">

                    <h3 class="text-success"><?php echo e(Session::get('alert-success')); ?></h3>
                    <h3 class="text-danger"><?php echo e(Session::get('alert-danger')); ?></h3>

                    <p><a class="pull-right btn btn-success" href="/cadastro-visitante">Novo</a></p>
                    <table class="table">
                        <?php $__empty_1 = true; $__currentLoopData = $visitantes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $visitante): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); $__empty_1 = false; ?>
                            <tr>
                                <td>
                                    <a href="/edicao-visitante/<?php echo e($visitante->id); ?>"><?php echo e($visitante->nome); ?></a>
                                </td>
                                <td>
                                    <button class="btn btn-danger remover" value="<?php echo e($visitante->id); ?>">Remover</button>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); if ($__empty_1): ?>
                            <tr><td><h3>Nenhum Visitante Encontrado</h3></td></tr>
                        <?php endif; ?>
                    </table>
                    <?php echo e($visitantes->links()); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(url('/js/jquery.mask.min.js')); ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#cpf").mask("000.000.000-00");
        $("#telefone").mask("(00) 90000 0000");
        $("#data_nascimento").mask("00/00/0000");

        $(".remover").click(function(){
            if(confirm("Tem certeza?")){
                $.post("/deletar-visitante", {
                    id: $(this).val(),
                    "_token": "<?php echo e(csrf_token()); ?>"
                }, function(result){
                    if(result == ""){
                        location.reload();
                    }else{
                        alert(result);
                    }
                });
            }
        })
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>