<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Empresas</div>
                <div class="panel-body">

                    <h3 class="text-success"><?php echo e(Session::get('alert-success')); ?></h3>
                    <h3 class="text-danger"><?php echo e(Session::get('alert-danger')); ?></h3>

                    <p><a class="pull-right btn btn-success" href="/cadastro-empresa">Novo</a></p>
                    <table class="table">
                        <?php $__empty_1 = true; $__currentLoopData = $empresas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empresa): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); $__empty_1 = false; ?>
                            <tr>
                                <td>
                                    <a href="/edicao-empresa/<?php echo e($empresa->id); ?>"><?php echo e($empresa->nome); ?></a>
                                </td>
                                <td>
                                    <button class="btn btn-danger remover" value="<?php echo e($empresa->id); ?>">Remover</button>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); if ($__empty_1): ?>
                            <tr><td><h3>Nenhuma Empresa Encontrada</h3></td></tr>
                        <?php endif; ?>
                    </table>
                    <?php echo e($empresas->links()); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(url('/js/jquery.mask.min.js')); ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".remover").click(function(){
            if(confirm("Tem certeza?")){
                $.post("/deletar-empresa", {
                    id: $(this).val(),
                    "_token": "<?php echo e(csrf_token()); ?>"
                }, function(result){
                    if(result == ""){
                        location.reload();
                    }else{
                        alert(result);
                    }
                });
            }
        })
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>