<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Alteração de Usuários</div>
                <div class="panel-body">
                    <form action="" method="post">
                    	<?php echo e(csrf_field()); ?>

                    	<div class="form-group">
                    		<label for="nome">Nome</label>
                    		<input class="form-control" value="<?php echo e($usuario->nome); ?>" name="nome" id="nome" placeholder="Nome" />
                            <p class="text-danger">
                                <?php if($errors->has('nome')): ?>
                                    <?php echo e($errors->first('nome')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>

                    	<div class="form-group">
                            <label for="apartamento">Apartamento</label>
                            <select class="form-control" id="apartamento" name="id_apartamento">
                                <option value="">Selecione</option>
                                <?php $__currentLoopData = $apartamentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $apartamento): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option <?php echo e($usuario->id_apartamento == $apartamento->id ? "selected" : ""); ?> value="<?php echo e($apartamento->id); ?>"><?php echo e($apartamento->nome); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                            <p class="text-danger">
                                <?php if($errors->has('id_apartamento')): ?>
                                    <?php echo e($errors->first('id_apartamento')); ?>

                                <?php endif; ?>
                            </p>
                        </div>

                        <div class="form-group">
                    		<label for="empresa">Empresa</label>
                    		<select class="form-control" id="empresa" name="id_empresa">
                    			<option value="">Selecione</option>
                    			<?php $__currentLoopData = $empresas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empresa): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    				<option <?php echo e($usuario->id_empresa == $empresa->id ? "selected" : ""); ?> value="<?php echo e($empresa->id); ?>"><?php echo e($empresa->nome); ?></option>
                    			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    		</select>
                            <p class="text-danger">
                                <?php if($errors->has('id_empresa')): ?>
                                    <?php echo e($errors->first('id_empresa')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>

                        <div class="form-group">
                            <button class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>