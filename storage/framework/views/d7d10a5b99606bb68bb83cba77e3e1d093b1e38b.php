<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Visitas</div>
                <div class="panel-body">

                    <h3 class="text-success"><?php echo e(Session::get('alert-success')); ?></h3>
                    <h3 class="text-danger"><?php echo e(Session::get('alert-danger')); ?></h3>

                    
                    <table class="table table-striped table-hover">
                        <tr>
                            <th>Tipo de Visita</th>
                            <th>Destino</th>
                            <th>Apartamento</th>
                            <th>Localização</th>
                            <th>Data</th>
                            <th>Registrado por</th>
                        </tr>
                        <?php $__empty_1 = true; $__currentLoopData = $visitas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $visita): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); $__empty_1 = false; ?>
                            <?php
                                if(!empty($visita->id_usuario)){
                                    $tipo_visita = "Usuário/Morador";
                                    $title = "Visita para Usuário/Morador";
                                }else if(!empty($visita->id_empresa)){
                                    $tipo_visita = "Empresa";
                                    $title = "Visita para Empresa";
                                }else if(!empty($visita->id_apartamento)){
                                    $tipo_visita = "Apartamento";
                                    $title = "Visita para Apartamento";
                                }
                            ?>
                            <tr title="<?php echo e($title); ?>">
                                <th><b><?php echo e($tipo_visita); ?></b></th>
                                <?php if(!empty($visita->id_usuario)): ?>
                                    <td><?php echo e($visita->usuario_visita); ?></td>
                                    <td><?php echo e($visita->apartamento_usuario); ?></td>
                                    <td><?php echo e($visita->localizacao_usuario); ?></td>
                                <?php elseif(!empty($visita->id_empresa)): ?>
                                    <td><?php echo e($visita->empresa_visita); ?></td>
                                    <td><?php echo e($visita->apartamento_empresa); ?></td>
                                    <td><?php echo e($visita->localizacao_empresa); ?></td>
                                <?php elseif(!empty($visita->id_apartamento)): ?>
                                    <td><?php echo e($visita->apartamento_visita); ?></td>
                                    <td></td>
                                    <td><?php echo e($visita->localizacao_apartamento); ?></td>
                                <?php endif; ?>
                                <td><?php echo e(date("d/m/Y H:i:s", strtotime($visita->data))); ?></td>
                                <td><?php echo e($visita->user); ?></td>

                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); if ($__empty_1): ?>
                            <tr><td><h3>Nenhuma Visita Encontrada</h3></td></tr>
                        <?php endif; ?>
                    </table>
                    <?php echo e($visitas->links()); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(url('/js/jquery.mask.min.js')); ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".remover").click(function(){
            if(confirm("Tem certeza?")){
                $.post("/deletar-apartamento", {
                    id: $(this).val(),
                    "_token": "<?php echo e(csrf_token()); ?>"
                }, function(result){
                    if(result == ""){
                        location.reload();
                    }else{
                        alert(result);
                    }
                });
            }
        })
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>