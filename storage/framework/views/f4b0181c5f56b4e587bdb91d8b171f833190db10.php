<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registrar Visitante</div>
                <div class="panel-body">
                    <form id="pesquisa_apartamento">
                        <div class="form-group">
                            <label>Selecione o <b>Apartamento</b></label>
                            <select class="select_apartamento form-control">
                                <option value="">Selecione</option>
                                <?php $__currentLoopData = $apartamentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $apartamento): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option value="<?php echo e($apartamento->id); ?>"><?php echo e($apartamento->nome); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                            <h3 class="text-center localizacao_apartamento"></h3>
                        </div>
                    </form>
                    <form id="pesquisa_visitante">
                        <div class="form-group">
                            <label>Informe o <b>RG</b> e pressione a tecla <b>ENTER</b></label>
                            <input autocomplete="off" style="height: 8rem; font-size: 3rem;" class="col-sm-12 form-control" placeholder="RG" name="rg" id="campo_pesquisa" autofocus />
                        </div>
                    </form>
                    <div class="row">
                        <h3 class="text-center resultado"></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-cadastro" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Visitante não cadastrado</h4>
      </div>
    <form action="" method="post" id="cadastrar">
        <div class="modal-body">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
                <label for="nome">Nome</label>
                <input class="form-control" name="nome" id="nome" placeholder="Nome" />
                <p class="text-danger">
                    <?php if($errors->has('nome')): ?>
                        <?php echo e($errors->first('nome')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="rg">RG</label>
                <input class="form-control" name="rg" id="rg" placeholder="RG" />
                <p class="text-danger">
                    <?php if($errors->has('rg')): ?>
                        <?php echo e($errors->first('rg')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="cpf">CPF</label>
                <input class="form-control" name="cpf" id="cpf" placeholder="CPF" />
                <p class="text-danger">
                    <?php if($errors->has('cpf')): ?>
                        <?php echo e($errors->first('cpf')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="endereco">Endereço</label>
                <input class="form-control" name="endereco" id="endereco" placeholder="Endereço" />
                <p class="text-danger">
                    <?php if($errors->has('endereco')): ?>
                        <?php echo e($errors->first('endereco')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="cidade">Cidade</label>
                <input class="form-control" name="cidade" id="cidade" placeholder="Cidade" />
                <p class="text-danger">
                    <?php if($errors->has('cidade')): ?>
                        <?php echo e($errors->first('cidade')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="estado">Estado</label>
                <input class="form-control" name="estado" id="estado" placeholder="Estado" />
                <p class="text-danger">
                    <?php if($errors->has('estado')): ?>
                        <?php echo e($errors->first('estado')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="telefone">Telefone</label>
                <input class="form-control" name="telefone" id="telefone" placeholder="Telefone" />
                <p class="text-danger">
                    <?php if($errors->has('telefone')): ?>
                        <?php echo e($errors->first('telefone')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="sexo">Sexo</label>
                <select class="form-control" name="sexo" id="sexo">
                    <option value="">Selecione</option>
                    <option value="M">Masculino</option>
                    <option value="F">Feminino</option>
                </select>
                <p class="text-danger">
                    <?php if($errors->has('sexo')): ?>
                        <?php echo e($errors->first('sexo')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="data_nascimento">Data de nascimento</label>
                <input class="form-control" name="data_nascimento" id="data_nascimento" placeholder="__/__/____" />
                <p class="text-danger">
                    <?php if($errors->has('data_nascimento')): ?>
                        <?php echo e($errors->first('data_nascimento')); ?>

                    <?php endif; ?>
                </p>
            </div>

            <div class="form-group">
                <label for="apartamento">Apartamento</label>
                <select class="form-control" id="apartamento" name="id_apartamento">
                    <option value="">Selecione</option>
                    <?php $__currentLoopData = $apartamentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $apartamento): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option value="<?php echo e($apartamento->id); ?>"><?php echo e($apartamento->nome); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </select>
                <p class="text-danger">
                    <?php if($errors->has('id_apartamento')): ?>
                        <?php echo e($errors->first('id_apartamento')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="empresa">Empresa</label>
                <select class="form-control" id="empresa" name="id_empresa">
                    <option value="">Selecione</option>
                    <?php $__currentLoopData = $empresas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empresa): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option value="<?php echo e($empresa->id); ?>"><?php echo e($empresa->nome); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </select>
                <p class="text-danger">
                    <?php if($errors->has('id_empresa')): ?>
                        <?php echo e($errors->first('id_empresa')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <div class="form-group">
                <label for="observacao">Observação</label>
                <textarea class="form-control" id="observacao" name="observacao"></textarea>
                <p class="text-danger">
                    <?php if($errors->has('observacao')): ?>
                        <?php echo e($errors->first('observacao')); ?>

                    <?php endif; ?>
                </p>
            </div>
            <h2 class="text-center registrado"></h2>
            <h2 class="text-center resposta"></h2>
      </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Cadastrar e Registrar Visita</button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection("scripts"); ?>
<script type="text/javascript" src="<?php echo e(url('/js/jquery.mask.min.js')); ?>"></script>
<script type="text/javascript">
    function pesquisar(){
        $.post("/pesquisa", {
            rg: $("#campo_pesquisa").val(),
            "_token": "<?php echo e(csrf_token()); ?>"
        }, function(result){
            $(".resultado").html(result);
        });
    }

    function visitar(id){
        $.post("/visitar-apartamento", {
            id_usuario: id,
            id_apartamento: $(".select_apartamento").val(),
            "_token": "<?php echo e(csrf_token()); ?>"
        }, function(result){
            if(result.success != null && result.success != ""){
                alert(result.success);
                location.href = "/home";
            }

            if(result.error != null && result.error != ""){
                alert(result.error);
            }

            console.log(result);
        });
    }

    $(document).ready(function(){
        $("#cpf").mask("000.000.000-00");
        $("#telefone").mask("(00) 90000 0000");
        $("#data_nascimento").mask("00/00/0000");
        // $("#campo_pesquisa").focus();
        // $("#campo_pesquisa").blur(function(){
        //     $("#campo_pesquisa").focus();
        // });
        $("#pesquisa_visitante").submit(function(e){
            e.preventDefault();
            $(".registrado").html("");
            pesquisar();
        });

        $("#cadastrar").submit(function(e){
            e.preventDefault();
            $.post("/cadastro-visitante-ajax", {
                nome: $("#nome").val(),
                rg: $("#rg").val(),
                cpf: $("#cpf").val(),
                endereco: $("#endereco").val(),
                cidade: $("#cidade").val(),
                estado: $("#estado").val(),
                telefone: $("#telefone").val(),
                sexo: $("#sexo").val(),
                data_nascimento: $("#data_nascimento").val(),
                id_apartamento: $("#apartamento").val(),
                id_empresa: $("#empresa").val(),
                observacao: $("#observacao").val(),
                "_token": "<?php echo e(csrf_token()); ?>"
            }, function(result){
                if(result.success != "" && result.success != null){
                    $(".registrado").html(result.success);
                    $(".modal").modal("hide");
                    pesquisar();
                }

                if(result.error != "" && result.error != null){
                    alert(result.error);
                }
            });
        });

        $(".select_apartamento").change(function(){
            if($(".select_apartamento").val() != "" && $(".select_apartamento").val() != null){
                $.post("/getApartamento", {
                    id: $(".select_apartamento").val(),
                    "_token": "<?php echo e(csrf_token()); ?>"
                }, function(result){
                    if(result.success != "" && result.success != null){
                        // $(".apartamento_empresa").html("Apartamento: "+result.success.apartamento);
                        $(".localizacao_apartamento").html("Localizacao: "+result.success.localizacao);
                    }

                    if(result.error != "" && result.error != null){
                        alert(result.error);
                    }
                });
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>