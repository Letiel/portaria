<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registrar Visitante</div>

                <div class="panel-body">
                    <h3 class="text-success"><?php echo e(Session::get('alert-success')); ?></h3>
                    <h3 class="text-danger"><?php echo e(Session::get('alert-danger')); ?></h3>
                    <h2>Registrar visita em:</h2>
                    <div class="col-sm-12 btn-group">
                        <a href="/visita-empresa" class="btn btn-primary btn-lg" style="width: 33.3333333333%;">Empresa</a>
                        <a href="/visita-apartamento" class="btn btn-success btn-lg" style="width: 33.3333333333%;">Apartamento</a>
                        <a href="/visita-pessoa" class="btn btn-warning btn-lg" style="width: 33.3333333333%;">Pessoa</a>
                    </div>
                    <div class="col-sm-12">
                        <hr>
                        <h4>Última Visita:</h4>
                        <?php if(!empty($ultimo->id_usuario)): ?>
                            <p>Usuário/Morador: <b><?php echo e($ultimo->usuario_visita); ?></b></p>
                            <p>Apartamento: <b><?php echo e($ultimo->apartamento_usuario); ?></b></p>
                            <p>Localização: <b><?php echo e($ultimo->localizacao_usuario); ?></b></p>
                        <?php elseif(!empty($ultimo->id_empresa)): ?>
                            <p>Empresa: <b><?php echo e($ultimo->empresa_visita); ?></b></p>
                            <p>Apartamento: <b><?php echo e($ultimo->apartamento_empresa); ?></b></p>
                            <p>Localização: <b><?php echo e($ultimo->localizacao_empresa); ?></b></p>
                        <?php elseif(!empty($ultimo->id_apartamento)): ?>
                            <p>Apartamento: <b><?php echo e($ultimo->apartamento_visita); ?></b></p>
                            <p>Localização: <b><?php echo e($ultimo->localizacao_apartamento); ?></b></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("scripts"); ?>
<script type="text/javascript" src="<?php echo e(url('/js/jquery.mask.min.js')); ?>"></script>
<script type="text/javascript">
    var visitante;
    function pesquisar(){
        $.post("/pesquisa", {
            rg: $("#campo_pesquisa").val(),
            "_token": "<?php echo e(csrf_token()); ?>"
        }, function(result){
            $(".resultado").html(result);
        });
    }

    function visitar(id){
        $(".opcoes_visita").modal("show");
        visitante = id;
    }

    // $.post("/visitar", {
    //     id: id,
    //     "_token": "<?php echo e(csrf_token()); ?>"
    // }, function(result){
    //     $(".resultado").html(result);
    //      visitante = "";
    // });
    $(document).ready(function(){
        $("#cpf").mask("000.000.000-00");
        $("#telefone").mask("(00) 90000 0000");
        $("#data_nascimento").mask("00/00/0000");
        // $("#campo_pesquisa").focus();
        // $("#campo_pesquisa").blur(function(){
        //     $("#campo_pesquisa").focus();
        // });
        $("#pesquisa_visitante").submit(function(e){
            e.preventDefault();
            $(".registrado").html("");
            pesquisar();
        });

        $("#cadastrar").submit(function(e){
            e.preventDefault();
            $.post("/cadastro-visitante-ajax", {
                nome: $("#nome").val(),
                rg: $("#rg").val(),
                cpf: $("#cpf").val(),
                endereco: $("#endereco").val(),
                cidade: $("#cidade").val(),
                estado: $("#estado").val(),
                telefone: $("#telefone").val(),
                sexo: $("#sexo").val(),
                data_nascimento: $("#data_nascimento").val(),
                id_apartamento: $("#apartamento").val(),
                id_empresa: $("#empresa").val(),
                observacao: $("#observacao").val(),
                "_token": "<?php echo e(csrf_token()); ?>"
            }, function(result){
                if(result.success != "" && result.success != null){
                    $(".registrado").html(result.success);
                    $(".modal").modal("hide");
                    pesquisar();
                }

                if(result.error != "" && result.error != null){
                    alert(result.error);
                }
            });
        });

        $(".select_empresa").change(function(){
            if($(".select_empresa").val() != "" && $(".select_empresa").val() != null){
                $.post("/getEmpresa", {
                    id: $(".select_empresa").val(),
                    "_token": "<?php echo e(csrf_token()); ?>"
                }, function(result){
                    if(result.success != "" && result.success != null){
                        $(".apartamento_empresa").html("Apartamento: "+result.success.apartamento);
                        $(".localizacao_empresa").html("Localizacao: "+result.success.localizacao);
                    }

                    if(result.error != "" && result.error != null){
                        alert(result.error);
                    }
                });
            }
        });

        $(".select_apartamento").change(function(){
            if($(".select_apartamento").val() != "" && $(".select_apartamento").val() != null){
                $.post("/getApartamento", {
                    id: $(".select_apartamento").val(),
                    "_token": "<?php echo e(csrf_token()); ?>"
                }, function(result){
                    if(result.success != "" && result.success != null){
                        // $(".apartamento_empresa").html("Apartamento: "+result.success.apartamento);
                        $(".localizacao_apartamento").html("Localizacao: "+result.success.localizacao);
                    }

                    if(result.error != "" && result.error != null){
                        alert(result.error);
                    }
                });
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>