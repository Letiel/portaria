<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Operador</div>
                <div class="panel-body">
                    <form action="" method="post">
                    	<?php echo e(csrf_field()); ?>

                    	<div class="form-group">
                            <label for="nome">Nome</label>
                            <input value="<?php echo e(old('nome')); ?>" class="form-control" name="nome" id="nome" placeholder="Nome" />
                            <p class="text-danger">
                                <?php if($errors->has('nome')): ?>
                                    <?php echo e($errors->first('nome')); ?>

                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input value="<?php echo e(old('email')); ?>" class="form-control" name="email" id="email" placeholder="email" />
                            <p class="text-danger">
                                <?php if($errors->has('email')): ?>
                                    <?php echo e($errors->first('email')); ?>

                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="password">Senha</label>
                            <input type="password" class="form-control" name="senha" id="password" placeholder="******" />
                            <p class="text-danger">
                                <?php if($errors->has('senha')): ?>
                                    <?php echo e($errors->first('senha')); ?>

                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="form-group">
                    		<label for="password_confirm">Confirmação de Senha</label>
                    		<input type="password" class="form-control" name="confirmacao" id="password_confirm" placeholder="******" />
                            <p class="text-danger">
                                <?php if($errors->has('confirmacao')): ?>
                                    <?php echo e($errors->first('confirmacao')); ?>

                                <?php endif; ?>
                            </p>
                    	</div>

                        <div class="form-group">
                            <button class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>