@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Visitantes</div>
                <div class="panel-body">

                    <h3 class="text-success">{{ Session::get('alert-success') }}</h3>
                    <h3 class="text-danger">{{ Session::get('alert-danger') }}</h3>

                    <p><a class="pull-right btn btn-success" href="/cadastro-visitante">Novo</a></p>
                    <table class="table">
                        @forelse($visitantes as $visitante)
                            <tr>
                                <td>
                                    <a href="/edicao-visitante/{{$visitante->id}}">{{$visitante->nome}}</a>
                                </td>
                                <td>
                                    <button class="btn btn-danger remover" value="{{$visitante->id}}">Remover</button>
                                </td>
                            </tr>
                        @empty
                            <tr><td><h3>Nenhum Visitante Encontrado</h3></td></tr>
                        @endforelse
                    </table>
                    {{ $visitantes->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{url('/js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#cpf").mask("000.000.000-00");
        $("#telefone").mask("(00) 90000 0000");
        $("#data_nascimento").mask("00/00/0000");

        $(".remover").click(function(){
            if(confirm("Tem certeza?")){
                $.post("/deletar-visitante", {
                    id: $(this).val(),
                    "_token": "{{ csrf_token() }}"
                }, function(result){
                    if(result == ""){
                        location.reload();
                    }else{
                        alert(result);
                    }
                });
            }
        })
    });
</script>
@endsection