@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Operador</div>
                <div class="panel-body">
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input value="{{old('nome', $operador->name)}}" class="form-control" name="nome" id="nome" placeholder="Nome" />
                            <p class="text-danger">
                                @if($errors->has('nome'))
                                    {{ $errors->first('nome')}}
                                @endif
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input value="{{old('email', $operador->email)}}" class="form-control" name="email" id="email" placeholder="email" />
                            <p class="text-danger">
                                @if($errors->has('email'))
                                    {{ $errors->first('email')}}
                                @endif
                            </p>
                        </div>

                        <h3>Gerar nova senha</h3>
                        <h4><small>*Deixe em branco para não alterar</small></h4>
                        <div class="form-group">
                            <label for="password">Senha</label>
                            <input type="password" class="form-control" name="senha" id="password" placeholder="******" />
                            <p class="text-danger">
                                @if($errors->has('senha'))
                                    {{ $errors->first('senha')}}
                                @endif
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="password_confirm">Confirmação de Senha</label>
                            <input type="password" class="form-control" name="confirmacao" id="password_confirm" placeholder="******" />
                            <p class="text-danger">
                                @if($errors->has('confirmacao'))
                                    {{ $errors->first('confirmacao')}}
                                @endif
                            </p>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection