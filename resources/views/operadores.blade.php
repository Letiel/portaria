@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Operadores do Sistema</div>
                <div class="panel-body">

                    <h3 class="text-success">{{ Session::get('alert-success') }}</h3>
                    <h3 class="text-danger">{{ Session::get('alert-danger') }}</h3>

                    <p><a class="pull-right btn btn-success" href="/cadastro-operador">Novo</a></p>
                    <table class="table">
                        @forelse($operadores as $operador)
                            <tr>
                                <td>
                                    <a href="/edicao-operador/{{$operador->id}}">{{$operador->name}}</a>
                                </td>
                                <td>
                                    <button class="btn btn-danger remover" value="{{$operador->id}}">Remover</button>
                                </td>
                            </tr>
                        @empty
                            <tr><td><h3>Nenhum Usuário Encontrado</h3></td></tr>
                        @endforelse
                    </table>
                    {{ $operadores->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{url('/js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".remover").click(function(){
            if(confirm("Tem certeza?")){
                $.post("/deletar-operador", {
                    id: $(this).val(),
                    "_token": "{{ csrf_token() }}"
                }, function(result){
                    if(result == ""){
                        location.reload();
                    }else{
                        alert(result);
                    }
                });
            }
        })
    });
</script>
@endsection