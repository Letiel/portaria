@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Visitantes</div>
                <div class="panel-body">
                    <form action="" method="post">
                    	{{ csrf_field() }}
                    	<div class="form-group">
                    		<label for="nome">Nome</label>
                    		<input class="form-control" name="nome" id="nome" placeholder="Nome" />
                            <p class="text-danger">
                                @if($errors->has('nome'))
                                    {{ $errors->first('nome')}}
                                @endif
                            </p>
                    	</div>

                    	<div class="form-group">
                    		<label for="localizacao">Localização</label>
                    		<select class="form-control" id="localizacao" name="id_localizacao">
                    			<option value="">Selecione</option>
                    			@foreach($localizacoes as $localizacao)
                    				<option value="{{$localizacao->id}}">{{$localizacao->nome}}</option>
                    			@endforeach
                    		</select>
                            <p class="text-danger">
                                @if($errors->has('id_apartamento'))
                                    {{ $errors->first('id_apartamento')}}
                                @endif
                            </p>
                    	</div>

                        <div class="form-group">
                            <button class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection