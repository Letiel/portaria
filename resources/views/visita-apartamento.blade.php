@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registrar Visitante</div>
                <div class="panel-body">
                    <form id="pesquisa_apartamento">
                        <div class="form-group">
                            <label>Selecione o <b>Apartamento</b></label>
                            <select class="select_apartamento form-control">
                                <option value="">Selecione</option>
                                @foreach($apartamentos as $apartamento)
                                    <option value="{{$apartamento->id}}">{{$apartamento->nome}}</option>
                                @endforeach
                            </select>
                            <h3 class="text-center localizacao_apartamento"></h3>
                        </div>
                    </form>
                    <form id="pesquisa_visitante">
                        <div class="form-group">
                            <label>Informe o <b>RG</b> e pressione a tecla <b>ENTER</b></label>
                            <input autocomplete="off" style="height: 8rem; font-size: 3rem;" class="col-sm-12 form-control" placeholder="RG" name="rg" id="campo_pesquisa" autofocus />
                        </div>
                    </form>
                    <div class="row">
                        <h3 class="text-center resultado"></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-cadastro" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Visitante não cadastrado</h4>
      </div>
    <form action="" method="post" id="cadastrar">
        <div class="modal-body">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nome">Nome</label>
                <input class="form-control" name="nome" id="nome" placeholder="Nome" />
                <p class="text-danger">
                    @if($errors->has('nome'))
                        {{ $errors->first('nome')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="rg">RG</label>
                <input class="form-control" name="rg" id="rg" placeholder="RG" />
                <p class="text-danger">
                    @if($errors->has('rg'))
                        {{ $errors->first('rg')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="cpf">CPF</label>
                <input class="form-control" name="cpf" id="cpf" placeholder="CPF" />
                <p class="text-danger">
                    @if($errors->has('cpf'))
                        {{ $errors->first('cpf')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="endereco">Endereço</label>
                <input class="form-control" name="endereco" id="endereco" placeholder="Endereço" />
                <p class="text-danger">
                    @if($errors->has('endereco'))
                        {{ $errors->first('endereco')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="cidade">Cidade</label>
                <input class="form-control" name="cidade" id="cidade" placeholder="Cidade" />
                <p class="text-danger">
                    @if($errors->has('cidade'))
                        {{ $errors->first('cidade')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="estado">Estado</label>
                <input class="form-control" name="estado" id="estado" placeholder="Estado" />
                <p class="text-danger">
                    @if($errors->has('estado'))
                        {{ $errors->first('estado')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="telefone">Telefone</label>
                <input class="form-control" name="telefone" id="telefone" placeholder="Telefone" />
                <p class="text-danger">
                    @if($errors->has('telefone'))
                        {{ $errors->first('telefone')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="sexo">Sexo</label>
                <select class="form-control" name="sexo" id="sexo">
                    <option value="">Selecione</option>
                    <option value="M">Masculino</option>
                    <option value="F">Feminino</option>
                </select>
                <p class="text-danger">
                    @if($errors->has('sexo'))
                        {{ $errors->first('sexo')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="data_nascimento">Data de nascimento</label>
                <input class="form-control" name="data_nascimento" id="data_nascimento" placeholder="__/__/____" />
                <p class="text-danger">
                    @if($errors->has('data_nascimento'))
                        {{ $errors->first('data_nascimento')}}
                    @endif
                </p>
            </div>

            <div class="form-group">
                <label for="apartamento">Apartamento</label>
                <select class="form-control" id="apartamento" name="id_apartamento">
                    <option value="">Selecione</option>
                    @foreach($apartamentos as $apartamento)
                        <option value="{{$apartamento->id}}">{{$apartamento->nome}}</option>
                    @endforeach
                </select>
                <p class="text-danger">
                    @if($errors->has('id_apartamento'))
                        {{ $errors->first('id_apartamento')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="empresa">Empresa</label>
                <select class="form-control" id="empresa" name="id_empresa">
                    <option value="">Selecione</option>
                    @foreach($empresas as $empresa)
                        <option value="{{$empresa->id}}">{{$empresa->nome}}</option>
                    @endforeach
                </select>
                <p class="text-danger">
                    @if($errors->has('id_empresa'))
                        {{ $errors->first('id_empresa')}}
                    @endif
                </p>
            </div>
            <div class="form-group">
                <label for="observacao">Observação</label>
                <textarea class="form-control" id="observacao" name="observacao"></textarea>
                <p class="text-danger">
                    @if($errors->has('observacao'))
                        {{ $errors->first('observacao')}}
                    @endif
                </p>
            </div>
            <h2 class="text-center registrado"></h2>
            <h2 class="text-center resposta"></h2>
      </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Cadastrar e Registrar Visita</button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section("scripts")
<script type="text/javascript" src="{{url('/js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
    function pesquisar(){
        $.post("/pesquisa", {
            rg: $("#campo_pesquisa").val(),
            "_token": "{{ csrf_token() }}"
        }, function(result){
            $(".resultado").html(result);
        });
    }

    function visitar(id){
        $.post("/visitar-apartamento", {
            id_usuario: id,
            id_apartamento: $(".select_apartamento").val(),
            "_token": "{{ csrf_token() }}"
        }, function(result){
            if(result.success != null && result.success != ""){
                alert(result.success);
                location.href = "/home";
            }

            if(result.error != null && result.error != ""){
                alert(result.error);
            }

            console.log(result);
        });
    }

    $(document).ready(function(){
        $("#cpf").mask("000.000.000-00");
        $("#telefone").mask("(00) 90000 0000");
        $("#data_nascimento").mask("00/00/0000");
        // $("#campo_pesquisa").focus();
        // $("#campo_pesquisa").blur(function(){
        //     $("#campo_pesquisa").focus();
        // });
        $("#pesquisa_visitante").submit(function(e){
            e.preventDefault();
            $(".registrado").html("");
            pesquisar();
        });

        $("#cadastrar").submit(function(e){
            e.preventDefault();
            $.post("/cadastro-visitante-ajax", {
                nome: $("#nome").val(),
                rg: $("#rg").val(),
                cpf: $("#cpf").val(),
                endereco: $("#endereco").val(),
                cidade: $("#cidade").val(),
                estado: $("#estado").val(),
                telefone: $("#telefone").val(),
                sexo: $("#sexo").val(),
                data_nascimento: $("#data_nascimento").val(),
                id_apartamento: $("#apartamento").val(),
                id_empresa: $("#empresa").val(),
                observacao: $("#observacao").val(),
                "_token": "{{ csrf_token() }}"
            }, function(result){
                if(result.success != "" && result.success != null){
                    $(".registrado").html(result.success);
                    $(".modal").modal("hide");
                    pesquisar();
                }

                if(result.error != "" && result.error != null){
                    alert(result.error);
                }
            });
        });

        $(".select_apartamento").change(function(){
            if($(".select_apartamento").val() != "" && $(".select_apartamento").val() != null){
                $.post("/getApartamento", {
                    id: $(".select_apartamento").val(),
                    "_token": "{{ csrf_token() }}"
                }, function(result){
                    if(result.success != "" && result.success != null){
                        // $(".apartamento_empresa").html("Apartamento: "+result.success.apartamento);
                        $(".localizacao_apartamento").html("Localizacao: "+result.success.localizacao);
                    }

                    if(result.error != "" && result.error != null){
                        alert(result.error);
                    }
                });
            }
        });
    });
</script>
@endsection
