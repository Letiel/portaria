@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Visitantes</div>
                <div class="panel-body">
                    <form action="" method="post">
                    	{{ csrf_field() }}
                    	<div class="form-group">
                    		<label for="nome">Nome</label>
                    		<input class="form-control" name="nome" id="nome" placeholder="Nome" />
                            <p class="text-danger">
                                @if($errors->has('nome'))
                                    {{ $errors->first('nome')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="rg">RG</label>
                    		<input class="form-control" name="rg" id="rg" placeholder="RG" />
                            <p class="text-danger">
                                @if($errors->has('rg'))
                                    {{ $errors->first('rg')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="cpf">CPF</label>
                    		<input class="form-control" name="cpf" id="cpf" placeholder="CPF" />
                            <p class="text-danger">
                                @if($errors->has('cpf'))
                                    {{ $errors->first('cpf')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="endereco">Endereço</label>
                    		<input class="form-control" name="endereco" id="endereco" placeholder="Endereço" />
                            <p class="text-danger">
                                @if($errors->has('endereco'))
                                    {{ $errors->first('endereco')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="cidade">Cidade</label>
                    		<input class="form-control" name="cidade" id="cidade" placeholder="Cidade" />
                            <p class="text-danger">
                                @if($errors->has('cidade'))
                                    {{ $errors->first('cidade')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="estado">Estado</label>
                    		<input class="form-control" name="estado" id="estado" placeholder="Estado" />
                            <p class="text-danger">
                                @if($errors->has('estado'))
                                    {{ $errors->first('estado')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="telefone">Telefone</label>
                    		<input class="form-control" name="telefone" id="telefone" placeholder="Telefone" />
                            <p class="text-danger">
                                @if($errors->has('telefone'))
                                    {{ $errors->first('telefone')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="sexo">Sexo</label>
                    		<select class="form-control" name="sexo" id="sexo">
                    			<option value="">Selecione</option>
                    			<option value="M">Masculino</option>
                    			<option value="F">Feminino</option>
                    		</select>
                            <p class="text-danger">
                                @if($errors->has('sexo'))
                                    {{ $errors->first('sexo')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="data_nascimento">Data de nascimento</label>
                    		<input class="form-control" name="data_nascimento" id="data_nascimento" placeholder="__/__/____" />
                            <p class="text-danger">
                                @if($errors->has('data_nascimento'))
                                    {{ $errors->first('data_nascimento')}}
                                @endif
                            </p>
                    	</div>

                    	<div class="form-group">
                    		<label for="apartamento">Apartamento</label>
                    		<select class="form-control" id="apartamento" name="id_apartamento">
                    			<option value="">Selecione</option>
                    			@foreach($apartamentos as $apartamento)
                    				<option value="{{$apartamento->id}}">{{$apartamento->nome}}</option>
                    			@endforeach
                    		</select>
                            <p class="text-danger">
                                @if($errors->has('id_apartamento'))
                                    {{ $errors->first('id_apartamento')}}
                                @endif
                            </p>
                    	</div>
                    	<div class="form-group">
                    		<label for="empresa">Empresa</label>
                    		<select class="form-control" id="empresa" name="id_empresa">
                    			<option value="">Selecione</option>
                    			@foreach($empresas as $empresa)
                    				<option value="{{$empresa->id}}">{{$empresa->nome}}</option>
                    			@endforeach
                    		</select>
                            <p class="text-danger">
                                @if($errors->has('id_empresa'))
                                    {{ $errors->first('id_empresa')}}
                                @endif
                            </p>
                    	</div>
                        <div class="form-group">
                            <label for="observacao">Observação</label>
                            <textarea class="form-control" id="observacao" name="observacao"></textarea>
                            <p class="text-danger">
                                @if($errors->has('observacao'))
                                    {{ $errors->first('observacao')}}
                                @endif
                            </p>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{url('/js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
    $("#cpf").mask("000.000.000-00");
    $("#telefone").mask("(00) 90000 0000");
    $("#data_nascimento").mask("00/00/0000");
</script>
@endsection