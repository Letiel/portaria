@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Visitas</div>
                <div class="panel-body">

                    <h3 class="text-success">{{ Session::get('alert-success') }}</h3>
                    <h3 class="text-danger">{{ Session::get('alert-danger') }}</h3>

                    
                    <table class="table table-striped table-hover">
                        <tr>
                            <th>Tipo de Visita</th>
                            <th>Destino</th>
                            <th>Apartamento</th>
                            <th>Localização</th>
                            <th>Data</th>
                            <th>Registrado por</th>
                        </tr>
                        @forelse($visitas as $visita)
                            <?php
                                if(!empty($visita->id_usuario)){
                                    $tipo_visita = "Usuário/Morador";
                                    $title = "Visita para Usuário/Morador";
                                }else if(!empty($visita->id_empresa)){
                                    $tipo_visita = "Empresa";
                                    $title = "Visita para Empresa";
                                }else if(!empty($visita->id_apartamento)){
                                    $tipo_visita = "Apartamento";
                                    $title = "Visita para Apartamento";
                                }
                            ?>
                            <tr title="{{$title}}">
                                <th><b>{{$tipo_visita}}</b></th>
                                @if(!empty($visita->id_usuario))
                                    <td>{{$visita->usuario_visita}}</td>
                                    <td>{{$visita->apartamento_usuario}}</td>
                                    <td>{{$visita->localizacao_usuario}}</td>
                                @elseif(!empty($visita->id_empresa))
                                    <td>{{$visita->empresa_visita}}</td>
                                    <td>{{$visita->apartamento_empresa}}</td>
                                    <td>{{$visita->localizacao_empresa}}</td>
                                @elseif(!empty($visita->id_apartamento))
                                    <td>{{$visita->apartamento_visita}}</td>
                                    <td></td>
                                    <td>{{$visita->localizacao_apartamento}}</td>
                                @endif
                                <td>{{date("d/m/Y H:i:s", strtotime($visita->data))}}</td>
                                <td>{{$visita->user}}</td>

                            </tr>
                        @empty
                            <tr><td><h3>Nenhuma Visita Encontrada</h3></td></tr>
                        @endforelse
                    </table>
                    {{ $visitas->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{url('/js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".remover").click(function(){
            if(confirm("Tem certeza?")){
                $.post("/deletar-apartamento", {
                    id: $(this).val(),
                    "_token": "{{ csrf_token() }}"
                }, function(result){
                    if(result == ""){
                        location.reload();
                    }else{
                        alert(result);
                    }
                });
            }
        })
    });
</script>
@endsection